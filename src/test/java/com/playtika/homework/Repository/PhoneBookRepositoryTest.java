package com.playtika.homework.Repository;

import com.playtika.homework.models.Contact;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PhoneBookRepositoryTest {

    private Repository phoneBookRepository = new PhoneBookRepository();

    @BeforeEach
    void init(){
        var phoneNumbers = new ArrayList<String>();
        phoneNumbers.add("093");
        var emails = new ArrayList<String>();
        phoneNumbers.add("qwerty");
        var contactList = new ArrayList<Contact>();
        contactList.add(new Contact( "Max", phoneNumbers, emails, ""));

        phoneBookRepository.savePhoneBook(contactList);
    }
    @org.junit.jupiter.api.Test
    void savePhoneBook() {
        var contacts = phoneBookRepository.getPhoneBook();

        contacts.remove(0);

        phoneBookRepository.savePhoneBook(contacts);

        assertEquals(phoneBookRepository.getPhoneBook().stream().count(), 0);
    }

    @org.junit.jupiter.api.Test
    void getPhoneBook() {
        var contacts = phoneBookRepository.getPhoneBook();

        assertEquals(contacts.stream().count(), 1);
    }
}