package com.playtika.homework.commands;

import com.playtika.homework.Repository.PhoneBookRepository;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.AddUserInput;
import com.playtika.homework.input.Input;
import com.playtika.homework.models.ContactInputModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AddNewContactCommandTest {
    Input userInput;
    ContactInputModel addUserInput;
    Repository contactsRepository = new PhoneBookRepository();
    AddNewContactCommand addNewContactCommand;

    @BeforeEach
    void setUp() {
        userInput = mock(AddUserInput.class);
        addUserInput = new ContactInputModel();
        addUserInput.setName("Max");
        addUserInput.setPhoneNumbers(Arrays.asList("218585015"));
        addUserInput.setEmails(Arrays.asList("sobaka@sobka"));
    }

    @Test
    void shouldAddNewContact() {
        when(userInput.GetUserInput()).thenReturn(addUserInput);

        addNewContactCommand = new AddNewContactCommand(contactsRepository, userInput);

        addNewContactCommand.Execute();

        var dataForAssert = contactsRepository.getPhoneBook();

        assertEquals(dataForAssert.get(0).getName(), "Max");
    }
}