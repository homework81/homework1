package com.playtika.homework.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.PhoneBookRepository;
import com.playtika.homework.Repository.Repository;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GetAllContactsCommandTest {

    Repository contactsRepository = new PhoneBookRepository();
    GetAllContactsCommand getAllContactsCommand;

    @Test
    void shouldReturnAllContacts() throws JsonProcessingException {
        Contact contact = new Contact("Max", Arrays.asList("1"), Arrays.asList("max@l"), null );
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        contactsRepository.savePhoneBook(contactList);
        getAllContactsCommand = new GetAllContactsCommand(contactsRepository);

        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        var resultForAssert = getAllContactsCommand.Execute();

        assertEquals(resultForAssert, Obj.writeValueAsString(contactList));

    }
}