package com.playtika.homework.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.PhoneBookRepository;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.GetContactByNameInput;
import com.playtika.homework.input.Input;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GetContactByNameCommandTest {
    Input userInput;
    GetContactByNameInput getContactByNameInput;
    Repository contactsRepository = new PhoneBookRepository();
    GetContactByNameCommand getContactByNameCommand;
    @BeforeEach
    void setUp() {
        userInput = mock(GetContactByNameInput.class);
    }

    @Test
    void shouldReturnContactByName() throws JsonProcessingException {
        when(userInput.GetUserInput()).thenReturn("Max");

        getContactByNameCommand = new GetContactByNameCommand(contactsRepository, userInput);
        Contact contact = new Contact("Max", Arrays.asList("1"), Arrays.asList("max@l"), null );
        Contact contact1 = new Contact("Aloha", Arrays.asList("1"), Arrays.asList("max@l"), null );
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        contactsRepository.savePhoneBook(contactList);

        var dataForAssert = getContactByNameCommand.Execute();

        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        assertEquals(dataForAssert, Obj.writeValueAsString(Arrays.asList(contact)));
    }
}