package com.playtika.homework.commands;

import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.PhoneBookRepository;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.DeleteByIdInput;
import com.playtika.homework.input.Input;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DeleteByIdCommandTest {

    Input userInput;
    Repository contactsRepository = new PhoneBookRepository();
    DeleteByIdCommand deleteByIdCommand;

    @BeforeEach
    void setUp() {
        userInput = mock(DeleteByIdInput.class);
    }

    @Test
    void shouldDeleteById() {
        Contact contact = new Contact("Max", Arrays.asList("1"), Arrays.asList("max@l"), null );

        when(userInput.GetUserInput()).thenReturn(contact.getId());

        deleteByIdCommand = new DeleteByIdCommand(contactsRepository, userInput);

        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        contactsRepository.savePhoneBook(contactList);

        deleteByIdCommand.Execute();

        var dataForAssert = contactsRepository.getPhoneBook();

        assertEquals(dataForAssert.stream().count(), 0);

    }
}