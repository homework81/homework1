package com.playtika.homework.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.playtika.homework.models.Contact;
import org.junit.jupiter.api.*;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;
@WireMockTest(httpPort = 8888)
class CommandServiceTest {

    private CommandService commandService;
    private List<Contact> contactsList = new ArrayList<Contact>();

    @BeforeEach
    void init() {

        var contact = new Contact("Max", Arrays.asList("311241"), Arrays.asList("egaga@ag"), null);
        contactsList.add(contact);

        commandService = new CommandService(HttpClient.newBuilder().build(),
                new ObjectMapper(), "http://localhost:8888");
    }

    @Test
    void shouldReturnAllContacts() throws JsonProcessingException {
        stubCommandServiceResponse();
        var contacts = commandService.getAll();

        assertEquals(contacts.get(0).getPhoneNumbers().get(0),contactsList.get(0).getPhoneNumbers().get(0));

    }
     void stubCommandServiceResponse() throws JsonProcessingException {
        var contactsList = new ArrayList<Contact>();
        var contact = new Contact("Max", Arrays.asList("311241"), Arrays.asList("egaga@ag"), null);
        contactsList.add(contact);

        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var testList = Obj.writeValueAsString(Arrays.asList(contact));

        stubFor(get(urlEqualTo("/contacts")).willReturn(
                okJson(testList)
        ));
    }
}