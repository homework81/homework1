package com.playtika.homework.commands;

import com.playtika.homework.Repository.Repository;

public abstract class Command {
    protected final Repository phoneBookRepository;

    public Command(Repository phoneBookRepository){
        this.phoneBookRepository = phoneBookRepository;
    }
    public abstract String Execute();
}
