package com.playtika.homework.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.Input;
import com.playtika.homework.models.ContactInputModel;

import java.io.IOException;

public class AddNewContactCommand extends Command{

    private Input userInput;

    public AddNewContactCommand(Repository phoneBookRepository, Input userInput) {
        super(phoneBookRepository);
        this.userInput = userInput;
    }
    @Override
    public String Execute() {
        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            ContactInputModel contactData = (ContactInputModel)userInput.GetUserInput();
            if(contactData != null){
                var contactList = phoneBookRepository.getPhoneBook();
                var newContact = new Contact(
                        contactData.getName(),
                        contactData.getPhoneNumbers(),
                        contactData.getEmails(),
                        contactData.getNotes());

                contactList.add(newContact);

                return Obj.writeValueAsString(newContact);
            }

            return "Wrong input";
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
