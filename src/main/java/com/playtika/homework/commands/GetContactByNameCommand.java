package com.playtika.homework.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.Input;

import java.io.IOException;
import java.util.ArrayList;

public class GetContactByNameCommand extends Command{
    private final Input userInput;

    public GetContactByNameCommand(Repository phoneBookRepository, Input userInput){
        super(phoneBookRepository);
        this.userInput = userInput;
    }

    @Override
    public String Execute() {
        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            String name = (String)userInput.GetUserInput();
            var contactList = phoneBookRepository.getPhoneBook();
            var contactListToShow = new ArrayList<>();

            for (Contact contact  : contactList) {
                if (contact.getName().startsWith(name)) {
                    contactListToShow.add(contact);
                }
            }
            return Obj.writeValueAsString(contactListToShow);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
