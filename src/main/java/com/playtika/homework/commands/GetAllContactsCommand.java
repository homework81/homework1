package com.playtika.homework.commands;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.commands.Command;

import java.io.IOException;

public class GetAllContactsCommand extends Command {

    public GetAllContactsCommand(Repository phoneBookRepository){
        super(phoneBookRepository);
    }

    @Override
    public String Execute() {
        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            return Obj.writeValueAsString(phoneBookRepository.getPhoneBook());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
