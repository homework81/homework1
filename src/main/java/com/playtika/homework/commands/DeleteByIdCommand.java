package com.playtika.homework.commands;

import com.playtika.homework.models.Contact;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.input.Input;

import java.util.InputMismatchException;

public class DeleteByIdCommand extends Command {
    private final Input userInput;

    public DeleteByIdCommand(Repository phoneBookRepository, Input userInput) {
        super(phoneBookRepository);
        this.userInput = userInput;
    }

    @Override
    public String Execute() {
        var contactList = phoneBookRepository.getPhoneBook();

        try {
            long id = (long)userInput.GetUserInput();

            for (Contact contact : contactList) {
                if (contact.getId() == id) {
                    contactList.remove(contact);
                    phoneBookRepository.savePhoneBook(contactList);
                    return "Contact with " + id + " removed";
                }
            }
            return "Phone book doesn't contain contact with id= " + id;

        } catch (InputMismatchException exp) {
            return "Wrong input";
        }
    }
}
