package com.playtika.homework.Repository;

import com.playtika.homework.models.Contact;

import java.util.ArrayList;
import java.util.List;

public class PhoneBookRepository implements Repository{
    private List<Contact> phoneBook;

    public void savePhoneBook(List<Contact> phoneBookToSave){
        phoneBook = phoneBookToSave;
    }

    public List<Contact> getPhoneBook(){
        if(phoneBook == null)
            phoneBook = new ArrayList<>();

        return phoneBook;
    }
}
