package com.playtika.homework.Repository;

import com.playtika.homework.models.Contact;

import java.util.List;

public interface Repository {
    void savePhoneBook(List<Contact> phoneBookToSave);
    List<Contact> getPhoneBook();
}
