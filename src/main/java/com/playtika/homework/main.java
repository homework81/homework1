package com.playtika.homework;

import com.playtika.homework.Repository.PhoneBookRepository;
import com.playtika.homework.Repository.Repository;
import com.playtika.homework.commands.*;
import com.playtika.homework.input.AddUserInput;
import com.playtika.homework.input.DeleteByIdInput;
import com.playtika.homework.input.GetContactByNameInput;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Repository phoneBookRepository = new PhoneBookRepository();

        HashMap<Integer, Command> menuCommands = new HashMap<Integer, Command>();

        menuCommands.put(1, new GetAllContactsCommand(phoneBookRepository));
        menuCommands.put(2, new DeleteByIdCommand(phoneBookRepository, new DeleteByIdInput()));
        menuCommands.put(3, new AddNewContactCommand(phoneBookRepository, new AddUserInput()));
        menuCommands.put(4, new GetContactByNameCommand(phoneBookRepository, new GetContactByNameInput()));

        Scanner scanner = new Scanner(System.in);
        int option = 0;
        while (option != 9) {
            System.out.println("Press 1 to get all contacts in the phone book: ");
            System.out.println("Press 2 to delete contact by id");
            System.out.println("Press 3 to add new contact");
            System.out.println("Press 4 to get contact by name/part of name");
            System.out.println("Press 9 exit");

            if (scanner.hasNextInt()) {
                option = scanner.nextInt();
                var t = menuCommands.get(option).Execute();
                System.out.println(t);
            }
            else{
                scanner.next();
                System.out.println("Try  Again");
            }
        }
    }
}
