package com.playtika.homework.models;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Data
public class Contact {

    private long id;
    private String name;
    private List<String> phoneNumbers;
    private List<String> emails;
    private String notes;
    private long createdTimestamp;

    protected Contact(){

    }
    public Contact(String name, List<String> phoneNumbers, List<String> emails, String notes){
        this.id = System.currentTimeMillis() / 1000L;
        this.name = name;
        this.phoneNumbers = phoneNumbers;
        this.emails = emails;
        this.notes = notes;

        createdTimestamp = System.currentTimeMillis() / 1000L;
    }
}
