package com.playtika.homework.models;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;
@Getter @Setter
public class ContactInputModel {
    private String name;
    private List<String> phoneNumbers = new ArrayList<>();
    private List<String> emails = new ArrayList<>();
    private String notes = null;
}

