package com.playtika.homework.input;

import com.playtika.homework.models.ContactInputModel;

import java.util.ArrayList;
import java.util.Scanner;

public class AddUserInput implements Input{

    public  AddUserInput(){

    }
    @Override
    public Object GetUserInput() {
        Scanner scanner = new Scanner(System.in);
        ContactInputModel contactToAdd = new ContactInputModel();
        System.out.println("Adding new contact: ");
        boolean inputTracker = true;

        while (inputTracker) {
            try {
                System.out.println("Print contact name: ");
                contactToAdd.setName(scanner.nextLine());

                System.out.println("How many phones you want to add?");
                int phoneCount = scanner.nextInt();
                scanner.nextLine();

                var tempNumSaver = new ArrayList<String>();
                for (int i = 1; i <= phoneCount; i++){
                    System.out.println("Enter " + i + "phone number");
                    tempNumSaver.add(scanner.nextLine());
                }

                contactToAdd.setPhoneNumbers(tempNumSaver);
                System.out.println("How many emails you want to add?");
                int emailsCount = scanner.nextInt();
                scanner.nextLine();

                var tempEmailSaver = new ArrayList<String>();
                for (int i = 1; i <= emailsCount; i++){
                    System.out.println("Enter " + i + "email");
                    tempEmailSaver.add(scanner.nextLine());
                }
                contactToAdd.setEmails(tempEmailSaver);

                System.out.println("Would you like to add some notes: 1 or 0?");
                if(scanner.nextInt() == 1){
                    System.out.println("Print your notes: ");
                    scanner.nextLine();
                    contactToAdd.setNotes(scanner.nextLine());
                }

                inputTracker = false;
                return contactToAdd;
            } catch (NumberFormatException exp) {
                inputTracker = true;
                scanner.next();
            }
        }
        return null;
    }
}
