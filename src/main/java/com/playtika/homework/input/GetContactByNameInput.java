package com.playtika.homework.input;

import java.util.Scanner;

public class GetContactByNameInput implements Input{
    @Override
    public Object GetUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Print full name or part to find contact: ");
        boolean inputTracker = true;
        while (inputTracker) {
            try {
                String name = scanner.nextLine();
                inputTracker = false;
                return name;
            } catch (NumberFormatException exp) {
                System.out.println("Wrong input data");
                inputTracker = true;
            }
        }
        return null;
    }
}
