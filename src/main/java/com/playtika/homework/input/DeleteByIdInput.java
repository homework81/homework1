package com.playtika.homework.input;

import java.util.Scanner;

public class DeleteByIdInput implements Input{
    @Override
    public Object GetUserInput() {

        Scanner scanner = new Scanner(System.in);
        int option;
        System.out.println("Print id to delete: ");
        boolean inputTracker = true;
        while (inputTracker) {
            try {
                int id = scanner.nextInt();
                inputTracker = false;
                return id;
            } catch (NumberFormatException exp) {
                inputTracker = true;
                scanner.next();
            }
        }
        return null;
    }
}
