package com.playtika.homework.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.homework.models.Contact;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@RequiredArgsConstructor
public class CommandService {
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final String link;

    @SneakyThrows
    List<Contact> getAll(){
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(link + "/contacts"))
                .build();

        String body = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();

        return objectMapper.readValue(body, new TypeReference<List<Contact>>() {
        });
    }

}
